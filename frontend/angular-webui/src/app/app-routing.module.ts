import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game/game.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { animation } from '@angular/animations';
import {AppComponent} from "./app.component";

const routes: Routes = [
  { path: 'game', component: GameComponent, data: { title: 'Game', animation: 'isRight' } },
  { path: 'scoreboard', component: ScoreboardComponent, data: { title: 'Scoreboard', animation: 'isLeft'} },
  {path: 'home', component: AppComponent, data: { title: 'Home', animation: 'isLeft' } }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
