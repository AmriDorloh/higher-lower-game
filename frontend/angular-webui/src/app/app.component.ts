import { Component, HostBinding } from '@angular/core';
import {ActivatedRoute, Router, RouterOutlet} from '@angular/router';
import {slider} from './route-animations';
import {Globals} from "./globals";
import {HttpClient} from "@angular/common/http";
import {EbayService} from "./ebay.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    slider,
  ]
})


export class AppComponent {
  title = 'angular-webui';
  isLoading: boolean;
  globals: Globals;

  constructor(globals: Globals, private ebayService: EbayService) {
    this.globals = globals;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.globals.isDisplay = false;
    //get full list of all ebay products from all categories
    this.ebayService.getProductList().subscribe(list => {
      this.globals.productList = list;
      this.isLoading = false;
    });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  disableMenu(){
    this.globals.isDisplay = true;
  }
}

