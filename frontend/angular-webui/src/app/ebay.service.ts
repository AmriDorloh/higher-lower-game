import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';

export class Product {
  title: string;
  price: number;
  currency: string;
  galleryURL: string;
}

@Injectable({
  providedIn: 'root'
})
export class EbayService {

  // Category catalogue
  categories: string[] = ['smartphone', 'book', 'music', 'games', 'computer', 'beer', 'barbecue', 'furniture',
  'radiator', 'tool', 'card', 'car', 'bike', 'camping', 'travel', 'sports', 'costume', 'cloth', 'joke', 'fun', 'art',
  'apple'];

  // Variable to decide how many entries we get per API request
  maxEntries = '100';

  constructor(private http: HttpClient) {
  }

  // Method which does an API request for each category of our catalogue; returns summarized productList of each category
  getProductList(): Observable<any[]> {
    // The array, where the requested data gets gathered
    const observableBatch = [];

    // for loop to make an API request for each category
    this.categories.forEach((keyword) => {
      observableBatch.push(this.http.get('/posts/services/search/FindingService/v1?SECURITY-APPNAME=FynnPohl-HigherLo-PRD-5c8eaa0db-36bffb26&OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&keywords=' + keyword + '&paginationInput.entriesPerPage=' + this.maxEntries + '&GLOBAL-ID=EBAY-US&siteid=0&outputSelector(0)=PictureURLSuperSize '));
    });

    // make sure that all requests are done with fork join and return the final result
    return forkJoin(observableBatch);
  }


}



