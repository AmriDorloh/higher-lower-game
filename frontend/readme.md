# Frontend

## Requirements 
- npm install -g @angular/cli

## Run 
- Start Webapp with "ng serve"

## Database integration 

- Install Sqlite3 on machine if not installed
- Install Sqlite3 in Node: npm install sqlite3
- Create Database in sqlite3